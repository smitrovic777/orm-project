package com.strahinja;

import java.sql.SQLException;
import java.util.List;

/*
 * @author Strahinja Mitrovic
 */
public class ORMTool {

    public static void main(String[] args) throws SQLException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException, InstantiationException {
        Person p = new Person();
        System.out.println(p.getById(3));
        System.out.println(p.getFirstname());
        System.out.println(p.getId());
        System.out.println(p.getLastname());
        List<Person> list = p.getAll();
        for (Person person : list) {
            System.out.println(person.toString());
        }
    }

}
