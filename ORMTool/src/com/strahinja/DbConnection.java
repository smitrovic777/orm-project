/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Strahinja
 */
public class DbConnection {

    private static Connection instance;

    private DbConnection() {

    }

    public synchronized static Connection getConnection() throws SQLException {
        if (instance == null) {
            instance = DriverManager.getConnection("jdbc:mysql://localhost/persons", "root", "");
        }
        return instance;
    }

}
