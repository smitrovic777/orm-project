/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja;

/**
 *
 * @author Strahinja
 */
@ActiveRecordEntity(tableName = "person", keyColumnName = "id")
public class Person extends ActiveRecord{

    protected int id;
    protected String firstname;
    protected String lastname;

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int hashCode() {
        return this.getId() * this.getFirstname().hashCode() * this.getLastname().hashCode();
    }

    
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Person)) {
            return false;
        }

        Person p = (Person) obj;
        if (this.getId() != p.getId()) {
            return false;
        } else if (!(this.getFirstname().equals(p.getFirstname()))) {
            return false;
        } else if (!(this.getLastname().equals(p.getLastname()))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "ID: " + this.getId() + " FIRSTNAME: " + this.getFirstname() + " LASTNAME: " + this.getLastname();
    }

}
