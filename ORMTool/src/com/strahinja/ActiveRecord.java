/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.strahinja;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Strahinja
 */
public class ActiveRecord {

    public boolean delete(int id) throws SQLException {
        Class<ActiveRecord> c = (Class<ActiveRecord>) this.getClass();
        ActiveRecordEntity activeRecordEntityAnnotation = (ActiveRecordEntity) c.getAnnotation(ActiveRecordEntity.class);
        Connection conn = DbConnection.getConnection();
        PreparedStatement st = conn.prepareStatement("DELETE FROM " + activeRecordEntityAnnotation.tableName() + " WHERE " + activeRecordEntityAnnotation.keyColumnName() + "=?;");
        st.setInt(1, id);
        st.execute();
        return true;
    }

    public boolean insert() throws SQLException, IllegalArgumentException, IllegalAccessException {
        Class<ActiveRecord> c = (Class<ActiveRecord>) this.getClass();
        ActiveRecordEntity activeRecordEntityAnnotation = (ActiveRecordEntity) c.getAnnotation(ActiveRecordEntity.class);
        Connection conn = DbConnection.getConnection();
        Field[] fields = c.getDeclaredFields();
        StringBuilder query = new StringBuilder();
        query.append("INSERT INTO " + activeRecordEntityAnnotation.tableName() + " (");
        int fieldsCount = 0;
        for (Field field : fields) {
            if (field.getName().equals(activeRecordEntityAnnotation.keyColumnName())) {
                continue;
            }
            query.append(field.getName() + ",");
            fieldsCount++;
        }
        query.deleteCharAt(query.length() - 1);
        query.append(") VALUES (");
        for (int i = 0; i < fieldsCount; i++) {
            query.append("?,");
        }
        query.deleteCharAt(query.length() - 1);
        query.append(")");
        PreparedStatement st = conn.prepareStatement(query.toString());
        byte count = 1;
        for (Field field : fields) {
            if (field.getName().equals(activeRecordEntityAnnotation.keyColumnName())) {
                continue;
            }
            if (field.getType() == int.class) {
                st.setInt(count++, field.getInt(this));
            } else if (field.getType().equals(String.class)) {
                st.setString(count++, (String) field.get(this));
            }
        }
        st.execute();
        return true;
    }

    public boolean update(int id) throws SQLException, IllegalArgumentException, IllegalAccessException, NoSuchFieldException {
        Class<ActiveRecord> c = (Class<ActiveRecord>) this.getClass();
        ActiveRecordEntity activeRecordEntityAnnotation = (ActiveRecordEntity) c.getAnnotation(ActiveRecordEntity.class);
        Connection conn = DbConnection.getConnection();
        Field[] fields = c.getDeclaredFields();
        StringBuilder query = new StringBuilder();
        query.append("UPDATE " + activeRecordEntityAnnotation.tableName() + " SET ");
        int fieldCount = 0;
        for (Field f : fields) {
            if (f.getName().equals(activeRecordEntityAnnotation.keyColumnName())) {
                continue;
            }
            query.append(f.getName() + "=?,");
            fieldCount++;
        }
        query.deleteCharAt(query.length() - 1);
        query.append(" WHERE " + activeRecordEntityAnnotation.keyColumnName() + "=?");
        PreparedStatement st = conn.prepareStatement(query.toString());
        int count = 1;
        for (Field f : fields) {
            if (f.getName().equals(activeRecordEntityAnnotation.keyColumnName())) {
                continue;
            }
            if (f.getType() == int.class) {
                st.setInt(count++, f.getInt(this));
            } else if (f.getType().equals(String.class)) {
                st.setString(count++, (String) f.get(this));
            }
        }
        st.setInt(count, c.getDeclaredField(activeRecordEntityAnnotation.keyColumnName().toString()).getInt(this));
        st.execute();
        return true;
    }

    public boolean getById(int id) throws SQLException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Class<ActiveRecord> c = (Class<ActiveRecord>) this.getClass();
        ActiveRecordEntity activeRecordEntityAnnotation = (ActiveRecordEntity) c.getAnnotation(ActiveRecordEntity.class);
        Connection conn = DbConnection.getConnection();
        PreparedStatement st = conn.prepareStatement("SELECT * FROM " + activeRecordEntityAnnotation.tableName() + " WHERE " + activeRecordEntityAnnotation.keyColumnName() + "=?");
        st.setInt(1, id);
        ResultSet rs = st.executeQuery();
        if (!(rs.isBeforeFirst())) {
            return false;
        } else {
            rs.first();
            ResultSetMetaData metaData = rs.getMetaData();
            int count = metaData.getColumnCount();
            for (int i = 1; i <= count; i++) {
                Field f = c.getDeclaredField(metaData.getColumnName(i));
                if (f.getType() == int.class) {
                    f.set(this, rs.getInt(i));
                } else if (f.getType().equals(String.class)) {
                    f.set(this, rs.getString(i));
                }
            }
        }
        return true;
    }

    public <RESTYPE> List<RESTYPE> getAll() throws SQLException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        List<RESTYPE> result = new ArrayList<RESTYPE>();
        Class<ActiveRecord> c = (Class<ActiveRecord>) this.getClass();
        ActiveRecordEntity activeRecordEntityAnnotation = (ActiveRecordEntity) c.getAnnotation(ActiveRecordEntity.class);
        Connection conn = DbConnection.getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM " + activeRecordEntityAnnotation.tableName());
        if (!rs.isBeforeFirst()) {
            return result;
        } else {
            ResultSetMetaData metaData = rs.getMetaData();
            int count = metaData.getColumnCount();
            while (rs.next()) {
                RESTYPE r = (RESTYPE) c.newInstance();
                for (int i = 1; i <= count; i++) {
                    Field f = c.getDeclaredField(metaData.getColumnName(i));
                    if (f.getType() == int.class) {
                        f.set(r, rs.getInt(i));
                    } else if (f.getType().equals(String.class)) {
                        f.set(r, rs.getString(i));
                    }
                }
                result.add(r);
            }
        }
        return result;
    }
}
